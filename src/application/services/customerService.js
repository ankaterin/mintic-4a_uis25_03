const CustomerEntity = require('../../domain/entities/customerEntity');

class CustomerService {
  static async getAll({params}, res) {
    try {
      if (params.id) {
        const customers = await CustomerEntity.find({identification: params.id}).exec();

        if (!customers.length) return res.status(404).json({msg: 'Customer was not found'});
        return res.status(200).json(customers);
      }

      res.status(200).json(await CustomerEntity.find({}).exec());
    } catch (err) {
      res.status(500).json({msg: 'Internal error, try later ' + err.message});
    }
  }

  static async create({body}, res) {
    try {
      const customer = await CustomerEntity.find({identification: body.identification}).exec();

      if (customer.length)
        return res.status(400).json({
          msg: `The customer with identification: "${body.identification}" already existed in platform`,
        });

      const Customer = new CustomerEntity(body);
      Customer.save();
      res.status(200).json({msg: 'User inseted correctly'});
    } catch (err) {
      res.status(500).json({msg: 'Internal error, try later'});
    }
  }

  static async update({body, params}, res) {
    try {
      if (params.id) {
        const dataToUpdate = {
          name: body.name,
          lastName: body.lastName,
          email: body.email,
          phone: body.phone,
        };

        await CustomerEntity.findOneAndUpdate({identification: params.id}, dataToUpdate).exec();

        return res.status(200).json({...dataToUpdate, identification: params.id});
      }

      res.status(400).json({
        msg: 'Is necessary the identification',
        err: true,
      });
    } catch (err) {
      res.status(500).json({msg: 'Internal error, try later ' + err.message});
    }
  }

  static async delete({params}, res) {
    try {
      if (params.id) {
        await CustomerEntity.deleteOne({identification: params.id}).exec();

        return res.status(204).json();
      }

      res.status(400).json({
        msg: 'Is necessary the identification',
        err: true,
      });
    } catch (err) {
      res.status(500).json({msg: 'Internal error, try later ' + err.message});
    }
  }
}

module.exports = CustomerService;
