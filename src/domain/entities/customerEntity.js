const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const customerSchema = Schema({
  name: String,
  lastName: String,
  email: String,
  phone: String,
  identification: String,
});
const customer = mongoose.model('customer', customerSchema);
module.exports = customer;
