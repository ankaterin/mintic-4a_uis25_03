const {Router} = require('express');
const CustomerService = require('../../src/application/services/customerService');
const router = Router();

router.get('api/v1/customers/:id?', CustomerService.getAll);
router.post('api/v1/customers/', CustomerService.create);
router.put('api/v1/customers/:id', CustomerService.update);
router.delete('api/v1/customers/:id', CustomerService.delete);

module.exports = router;
